package android.ochoad.crystalball;


public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions(){
        answers = new String[] {
                "your wishes will come true.",
                "your wishes will NEVER come true"
        };

    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }
    public String getPredictions(){
        return answers[1];
    }
}
